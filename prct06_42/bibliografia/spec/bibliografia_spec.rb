require 'spec_helper'
require "lib/bibliografia.rb"

describe Bib do

  before :each do
    @p1 = Bib.new(['Juanito','Pepito'], ['El laberinto del fauno'], ['(Los fabunos cornudos)'], ['Programming Bookshelf'], ['4 edition'], 
    ['July 7, 2013'], ['ISBN-13: 978-1937785499', 'ISBN-10: 1937785491'])
  end    
    
  describe "Prueba de almacenamiento de variables:" do
    it "Se almacena al menos un autor" do
      @p1.autores.length.should_not be 0 
    end
    it "Se almacena un titulo" do
      @p1.titulo.length.should_not be 0 
    end
    it "Se almacena una serie" do
      @p1.serie.length.should_not be 0
    end
    it "Se almacena una editorial" do
      @p1.editorial.length.should_not be 0
    end
    it "Se almacena un numero de edicion" do
      @p1.edicion.length.should_not be 0
    end
    it "Se almacena una fecha de publicacion" do
      @p1.fecha.length.should_not be 0
    end
    it "Se almacena al menos un ISBN" do
      @p1.isbn.length.should_not be 0 
    end
  end
  
  describe "Prueba de existencia de los métodos: " do
      
    it "Existe el método get_autores" do
      @p1.respond_to?(:get_autores).should be true   
    end    

    it "Existe el método get_titulo" do
      @p1.respond_to?(:get_titulo).should be true   
    end        
      
    it "Existe el método get_serie" do
      @p1.respond_to?(:get_serie).should be true   
    end 
    
    it "Existe el método get_editorial" do
      @p1.respond_to?(:get_editorial).should be true   
    end
    
    it "Existe el método get_edicion" do
      @p1.respond_to?(:get_edicion).should be true   
    end
    
    it "Existe el método get_fecha" do
      @p1.respond_to?(:get_fecha).should be true   
    end 
    
    it "Existe el método get_isbn" do
      @p1.respond_to?(:get_isbn).should be true   
    end 

    it "Existe el método get_formateo" do
      @p1.respond_to?(:get_formateo).should be true   
    end     
  end  
end