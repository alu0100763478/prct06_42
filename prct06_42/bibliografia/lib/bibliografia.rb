require "bibliografia/version"


class Bib
  attr_reader :autores, :titulo, :serie, :editorial, :edicion, :fecha, :isbn
  def initialize(autores, titulo, serie, editorial, edicion, fecha, isbn)
    @autores = autores
    @titulo = titulo
    @serie = serie
    @editorial = editorial
    @edicion = edicion
    @fecha = fecha
    @isbn = isbn
  end
  
  def get_autores
    @autores
  end
  
  def get_titulo
    @titulo
  end 

  def get_serie
    @serie
  end 
  
  def get_editorial
    @editorial
  end 
  
  def get_edicion
    @edicion
  end   

  def get_fecha
    @fecha
  end
  
  def get_isbn
    @isbn
  end
  
  def get_formateo
    puts "Autor: #{@autores} \n Título: #{@titulo} \n Serie: #{@serie} \n #{@editorial}; #{@edicion} #{@fecha} \n ISBN: #{@isbn}"
  end  
end
